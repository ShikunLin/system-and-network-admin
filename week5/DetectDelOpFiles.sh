#!/bin/bash
procs=`sudo lsof | grep  '(deleted)' | awk '{print $2}'`

for proc in $procs
do
    sudo kill $proc
done

