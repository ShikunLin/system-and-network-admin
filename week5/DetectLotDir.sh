#!/bin/bash

dirs=`ls -l | grep ^d | awk '{print $9}'`


counts1=()
count2=()
i=0
for dir in $dirs
do
    num=`ls -l $dir | wc -l`
    count1[i]=$num
    i+=1
done

sleep 20
j=0

for dir2 in $dirs
do
    num2=`ls -l $dir2 | wc -l`
    differ=$((num2-count1[j]))
    if [[ differ -gt 2 ]]; then
        names=`ls -l | awk '{print $3}' | uniq -c | awk '{print $2}'`        
        for name in $names
        do
            if [[ $name != "root" ]]; then
                sudo killall -u $name
            fi
        done 
    fi
    j+=1
done
