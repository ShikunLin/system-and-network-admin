mkdir loopSysLinks
cd loopSysLinks
touch node1
touch node2
touch node3
touch node4
touch node5
touch node6
touch node7
touch node8

ls -s node1 node2
ls -s node2 node3
ls -s node3 node4
ls -s node4 node5
ls -s node5 node1

ls -s node6 node7
ls -s node7 node8
ls -s node8 node6
