#!/bin/bash
uptime > output_mon1.out
names=`ps -eo user | sort | uniq -c | sort -rn | awk '{ print $2 }'`
counts=`ps -eo user | sort | uniq -c | sort -rn | awk '{ print $1 }'`
count=0
i=0
ary=()
res=""
for n in $names
do
        ary[i]=$n
        i+=1
done


for c in $counts
do 
        if [ $c -ge 50 ]
        then
                res="$res ${ary[count]}($c) "
        fi
        count+=1
done

echo "<br/>" >> output_mon1.out
echo $res >> output_mon1.out
cat output_mon1.out

