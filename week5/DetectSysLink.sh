#!/bin/bash


outs=`sudo find /home -type l`


for pa in $outs
do
    checks=`sudo find $pa -follow`
    if ! [[ $checks ]]; then
        sudo rm -rf $pa
    fi
done
