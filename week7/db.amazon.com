;
; BIND data file for amazon.com
;
$TTL	604800
@	IN	SOA	www.amazon.com. root.www.amazon.com. (
			      2		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;Name Server Information
@	IN	NS	www.amazon.com.

www	IN	A	10.0.2.15
