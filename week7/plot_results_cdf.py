import matplotlib.pyplot as plt
import numpy as np
res1=[]
res2=[]
yaix=[]
count=0.0
with open("results_dnsperf.out","r") as f1:
	datas1 = f1.readlines()
	datas1 = [x.strip() for x in datas1] 

with open("round2_results_dnsperf.out","r") as f2:
	datas2 = f2.readlines()
	datas2 = [x.strip() for x in datas2] 

for data in datas1:
	line = data.split()
	ftime = float(line[3]) 
	res1.append(ftime)


for data in datas2:
	line = data.split()
	ftime = float(line[3]) 
	res2.append(ftime)


res1_sorted = sorted(res1)
res2_sorted = sorted(res2)

totoal_res1 = sum(res1)
totoal_res2 = sum(res2)


title1 = "{} {}s".format("First Round For dnsperf Command\ntotoal average latency =",totoal_res1)
title2 = "{} {}s".format("Second Round For dnsperf Command\ntotoal average latency =",totoal_res2)
print(title1)
for i in range(500):
	yaix.append(count)
	count+=0.002


plt.subplot(211)
plt.plot(res1_sorted,yaix, 'ro')
plt.title(title1)
my_x_ticks = np.arange(0, 5, 0.2)
my_y_ticks = np.arange(0, 1, 0.05)
plt.xticks(my_x_ticks)
plt.yticks(my_y_ticks)
plt.subplot(212)
plt.plot(res2_sorted,yaix, 'ro')
plt.title(title2)
plt.xticks(my_x_ticks)
plt.yticks(my_y_ticks)

plt.show()
#plt.savefig("round1_result.png",dpi=100)

