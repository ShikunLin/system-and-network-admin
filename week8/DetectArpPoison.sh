#!/bin/bash

while true; do


    fping -a -g 192.167.73.0/24
    nmap -n -sP 192.168.73.0/24
        
    sleep 60
    uptime >> detect_arp.log    
    ip neig show 192.168.73.0/24 >> detect_arp.log
    echo "==========================================" >> detect_arp.log    
done
