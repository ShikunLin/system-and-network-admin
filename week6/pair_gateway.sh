#!/bin/sh

iptables -t nat -A POSTROUTING -o eth0 ! -p esp -j SNAT --to-source 10.0.2.15

iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE
iptables -A FORWARD -i enp0s3 -o enp0s8 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i enp0s8 -o enp0s3 -j ACCEPT
